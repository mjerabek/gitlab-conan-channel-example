# Example project for https://gitlab.com/gitlab-org/gitlab/-/issues/216163

All CI jobs prefixed with "fail" should fail, others should succeed.
