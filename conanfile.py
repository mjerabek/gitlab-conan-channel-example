from conans import ConanFile

class ExampleConan(ConanFile):
    name = "example"
    url = "https://gitlab.com/mjerabek/gitlab-conan-channel-example/"
    homepage = url
    description = "Example for testing conan channels in gitlab conan repo."
    license = "MIT"
    generators = "cmake"
    settings = "os", "arch", "compiler", "build_type"
    version = "1.0"
    scm = {
        "type": "git",
        "url": "auto",
        "revision": "auto",
    }

    def build(self):
        pass

    def package(self):
        pass
